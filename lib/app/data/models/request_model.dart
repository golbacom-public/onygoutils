
import 'package:onygoutils/app/data/models/car_type_model.dart';
import 'package:onygoutils/app/data/models/real_rent_model.dart';
import 'package:onygoutils/app/data/models/user_model.dart';


class RequestRent {
  int? id;
  int? carTypeId;
  User? user;
  int? carId;
  Car? car;
  Location? location;
  Location? driverLocation;
  CarType? carType;
  Driver? driver;
  int? price;
  String? driverId;
  dynamic startDate;
  dynamic endDate;
  dynamic realEndDate;
  int? duration;
  dynamic realDuration;
  String? status;
  dynamic stops;

  RequestRent(
      {this.id,
        this.carTypeId,
        this.user,
        this.carId,
        this.car,
        this.location,
        this.driverLocation,
        this.carType,
        this.driver,
        this.price,
        this.driverId,
        this.startDate,
        this.endDate,
        this.duration,
        this.status});

  RequestRent.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    carTypeId = json['car_type_id'];
    user = json['user'] != null ? User?.fromJson(json['user']) : null;
    carId = json['car_id'];
    car = json['car'] != null ? Car?.fromJson(json['car']) : null;
    location =
    json['location'] != null ? Location?.fromJson(json['location']) : null;
    driverLocation =
    json['driver_location'] != null ? Location?.fromJson(json['driver_location']) : null;
    carType = json['car_type'] != null ? CarType?.fromJson(json['car_type']) : null;
    driver = json['driver'] != null ? Driver?.fromJson(json['driver']) : null;
    price = json['price'];
    driverId = json['driver_id'];
    startDate = json['start_date'];
    endDate = json['end_date'];
    duration = json['duration'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['car_type_id'] = carTypeId;
    if (user != null) {
      data['user'] = user?.toJson();
    }
    data['car_id'] = carId;
    if (car != null) {
      data['car'] = car?.toJson();
    }
    if (location != null) {
      data['location'] = location?.toJson();
    }
    if (driverLocation != null) {
      data['driver_location'] = driverLocation?.toJson();
    }
    if (carType != null) {
      data['car_type'] = carType?.toJson();
    }
    if (driver != null) {
      data['driver'] = driver?.toJson();
    }
    data['price'] = price;
    data['driver_id'] = driverId;
    data['start_date'] = startDate;
    data['end_date'] = endDate;
    data['duration'] = duration;
    data['status'] = status;
    return data;
  }
}

