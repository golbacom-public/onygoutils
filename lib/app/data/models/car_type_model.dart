class CarType {
  int? id;
  String? label;
  String? description;
  String? hourPrice;
  String? dailyPrice;
  dynamic createdAt;
  dynamic updatedAt;
  late List<CarType> _carsTypes;
  List<CarType> get carsTypes=>_carsTypes;


  CarType(
      {this.id,
        this.label,
        this.description,
        this.hourPrice,
        this.dailyPrice,
        this.createdAt,
        this.updatedAt});

  CarType.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    label = json['label'];
    description = json['description'];
    hourPrice = json['hour_price'];
    dailyPrice = json['daily_price'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];

    if (json['data'] != null) {
      json['data'].forEach((v) {
        _carsTypes.add(CarType.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['label'] = label;
    data['description'] = description;
    data['hour_price'] = hourPrice;
    data['daily_price'] = dailyPrice;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    return data;
  }

  static List<CarType> listFromJson(list) =>
      List<CarType>.from(list.map((x) => CarType.fromJson(x)));
}
