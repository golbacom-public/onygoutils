
import 'dart:ffi';

import 'package:onygoutils/app/data/models/user_model.dart';

import 'car_type_model.dart';


class RealRent {
  String? id;
  int? carTypeId;
  User? user;
  int? carId;
  Car? car;
  Location? location;
  Location? driverLocation;
  CarType? carType;
  Driver? driver;
  int? price;
  int? driverId;
  int? alreadyDeb=0;
  dynamic startDate;
  dynamic requestedDate;
  dynamic endDate;
  dynamic realEndDate;
  int? duration;
  dynamic realDuration;
  String? status;
  String? driverLat;
  String? driverLng;
  bool? endDateUpdated=false;
  dynamic stops;

  RealRent(
      {this.id,
        this.carTypeId,
        this.user,
        this.carId,
        this.car,
        this.location,
        this.driverLocation,
        this.carType,
        this.driver,
        this.price,
        this.driverId,
        this.alreadyDeb,
        this.startDate,
        this.requestedDate,
        this.endDate,
        this.realEndDate,
        this.duration,
        this.realDuration,
        this.endDateUpdated,
        this.status,
        this.driverLat,
        this.driverLng,
        this.stops});

  RealRent.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    carTypeId = json['car_type_id'];
    user = json['user'] != null ? User?.fromJson(json['user']) : null;
    carId = json['car_id'];
    car = json['car'] != null ? Car?.fromJson(json['car']) : null;
    location =
    json['location'] != null ? Location?.fromJson(json['location']) : null;
    driverLocation =
    json['driver_location'] != null ? Location?.fromJson(json['driver_location']) : null;
    carType =
    json['car_type'] != null ? CarType?.fromJson(json['car_type']) : null;
    driver = json['driver'] != null ? Driver?.fromJson(json['driver']) : null;
    price = json['price'];
    driverId = json['driver_id'];
    startDate = json['start_date'];
    alreadyDeb = json['already_deb'];
    requestedDate = json['requested_date'];
    endDate = json['end_date'];
    realEndDate = json['real_end_date'];
    duration = json['duration'];
    endDateUpdated = json['end_date_updated'] ?? false;
    realDuration = json['real_duration'];
    status = json['status'];
    driverLat = "${json['driver_lat']}";
    driverLng = "${json['driver_lng']}";
    stops = json['stops'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['car_type_id'] = carTypeId;
    if (user != null) {
      data['user'] = user?.toJson();
    }
    data['car_id'] = carId;
    if (car != null) {
      data['car'] = car?.toJson();
    }
    if (location != null) {
      data['location'] = location?.toJson();
    }
    if (driverLocation != null) {
      data['driver_location'] = driverLocation?.toJson();
    }
    if (carType != null) {
      data['car_type'] = carType?.toJson();
    }
    if (driver != null) {
      data['driver'] = driver?.toJson();
    }
    data['price'] = price;
    data['driver_id'] = driverId;
    data['start_date'] = startDate;
    data['requested_date'] = requestedDate;
    data['end_date'] = endDate;
    data['already_deb'] = alreadyDeb;
    data['real_end_date'] = realEndDate;
    data['duration'] = duration;
    data['real_duration'] = realDuration;
    data['status'] = status;
    data['end_date_updated'] = endDateUpdated;
    data['driver_lat'] = driverLat;
    data['driver_lng'] = driverLng;
    data['stops'] = stops;
    return data;
  }
}



class Car {
  int? id;
  dynamic label;
  String? immatriculation;
  String? description;
  String? carTypeId;
  CarType? carType;
  String? isAvailable;
  CarDriver? carDriver;

  Car(
      {this.id,
      this.label,
      this.immatriculation,
      this.description,
      this.carTypeId,
      this.carType,
      this.isAvailable,
      this.carDriver});

  Car.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    label = json['label'];
    immatriculation = json['immatriculation'];
    description = json['description'];
    carTypeId = json['car_type_id'];
    carType =
        json['car_type'] != null ? CarType?.fromJson(json['car_type']) : null;
    isAvailable = json['is_available'];
    carDriver = json['car_driver'] != null
        ? CarDriver?.fromJson(json['car_driver'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['label'] = label;
    data['immatriculation'] = immatriculation;
    data['description'] = description;
    data['car_type_id'] = carTypeId;
    if (carType != null) {
      data['car_type'] = carType?.toJson();
    }
    data['is_available'] = isAvailable;
    if (carDriver != null) {
      data['car_driver'] = carDriver?.toJson();
    }
    return data;
  }
}


class CarDriver {
  int? id;
  String? name;
  String? surname;
  dynamic contact;
  String? typePiece;
  String? photo;
  dynamic createdAt;
  String? pieceId;

  CarDriver(
      {this.id,
      this.name,
      this.surname,
      this.contact,
      this.typePiece,
      this.photo,
      this.createdAt,
      this.pieceId});

  CarDriver.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    surname = json['surname'];
    contact = json['contact'];
    typePiece = json['type_piece'];
    createdAt = json['created_at'];
    photo = json['photo'];
    pieceId = json['piece_id'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    data['surname'] = surname;
    data['contact'] = contact;
    data['photo'] = photo;
    data['type_piece'] = typePiece;
    data['created_at'] = createdAt;
    data['piece_id'] = pieceId;
    return data;
  }
}

class Location {
  int? id;
  String? latitude;
  String? longitude;
  String? details;
  String? createdAt;

  Location(
      {this.id, this.latitude, this.longitude, this.details, this.createdAt});

  Location.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    latitude = json['latitude'];
    longitude = json['longitude'];
    details = json['details'];
    createdAt = json['created_at'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['latitude'] = latitude;
    data['longitude'] = longitude;
    data['details'] = details;
    data['created_at'] = createdAt;
    return data;
  }
}

class Driver {
  int? id;
  String? name;
  dynamic surname;
  String? email;
  String? photo;
  String? phone;
  String? points;
  String? role;
  dynamic emailVerifiedAt;
  dynamic contact;
  String? available;
  String? typePiece;
  String? pieceId;
  dynamic deletedAt;
  dynamic createdAt;
  dynamic updatedAt;

  Driver(
      {this.id,
      this.name,
      this.surname,
      this.email,
      this.phone,
      this.role,
      this.photo,
      this.emailVerifiedAt,
      this.contact,
      this.points,
      this.available,
      this.typePiece,
      this.pieceId,
      this.deletedAt,
      this.createdAt,
      this.updatedAt});

  Driver.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    surname = json['surname'];
    email = json['email'];
    phone = json['phone'];
    points = json['points'];
    photo = json['photo'];
    role = json['role'];
    emailVerifiedAt = json['email_verified_at'];
    contact = json['contact'];
    available = json['available'];
    typePiece = json['type_piece'];
    pieceId = json['piece_id'];
    deletedAt = json['deleted_at'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    data['surname'] = surname;
    data['email'] = email;
    data['phone'] = phone;
    data['points'] = points;
    data['role'] = role;
    data['photo'] = photo;
    data['email_verified_at'] = emailVerifiedAt;
    data['contact'] = contact;
    data['available'] = available;
    data['type_piece'] = typePiece;
    data['piece_id'] = pieceId;
    data['deleted_at'] = deletedAt;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    return data;
  }

  static List<RealRent> listFromJson(list) =>
      List<RealRent>.from(list.map((x) => RealRent.fromJson(x)));
}
