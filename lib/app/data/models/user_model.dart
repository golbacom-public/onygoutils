class User {
  int? id;
  bool? verified;
  String? name;
  String? surname;
  String? phone;
  String? sexe;
  String? points;
  String? photo;
  String? balance;
  String? email;
  String? rents;
  String? token;
  String? refreshToken;
  dynamic updatedAt;
  String? prefs;

  User(
      {this.id,
      this.verified,
      this.name,
      this.surname,
        this.photo,
        this.balance,
      this.phone,
      this.points,
      this.sexe,
      this.email,
      this.rents,
      this.token,
      this.refreshToken,
      this.updatedAt,
      this.prefs});

  User.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    verified = json['verified'];
    name = json['name'];
    surname = json['surname'];
    sexe = json['sexe'];
    photo = json['photo'];
    balance = json['balance'];
    points = json['points'];
    phone = json['phone'];
    email = json['email'];
    rents = json['rents'];
    token = json['token'];
    refreshToken = json['refresh_token'];
    updatedAt = json['updated_at'];
    prefs = json['prefs'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['verified'] = verified;
    data['name'] = name;
    data['sexe'] = sexe;
    data['photo'] = photo;
    data['balance'] = balance;
    data['points'] = points;
    data['surname'] = surname;
    data['phone'] = phone;
    data['email'] = email;
    data['rents'] = rents;
    data['token'] = token;
    data['refresh_token'] = refreshToken;
    data['updated_at'] = updatedAt;
    data['prefs'] = prefs;
    return data;
  }
}
