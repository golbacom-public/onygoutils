import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get_connect/http/src/response/response.dart';

class ProviderHelper{
  static Response handleResponse(Response response) {
    Response _response = response;
    if(_response.hasError && _response.body != null && _response.body is !String) {
      if(_response.body.toString().startsWith("{error: ")){
        var msg="";
         Map errors_msg=_response.body['error'];
        errors_msg.forEach((key, value) {
          msg+="${value[0]}\n";
        });
        _response = Response(statusCode: _response.statusCode, body: _response.body, statusText: msg);
      }else {
        _response = Response(statusCode: _response.statusCode, body: _response.body, statusText: _response.body['message']);
      }
    }else if(_response.hasError && _response.body == null) {
      Fluttertoast.showToast(msg: 'Connexion au server a échoué. Veuillez vérifier votre connexion internet.',
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0
      );
      _response = const Response(statusCode: 0,
          statusText: 'Connexion au server a échoué. Veuillez vérifier votre connexion internet.');
    }
    return _response;
  }
}