class OnygoConstants{
  static const DEFAULT_TIME_DURATION=Duration(hours: 1);
  static const ALERT_TIME_DURATION=Duration(minutes: 15);
  static const PHONE_KEY='phone';
}