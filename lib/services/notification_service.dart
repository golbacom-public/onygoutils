import 'package:flutter/foundation.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

class NotificationService {
  static final NotificationService _notificationService =
  NotificationService._internal();

  final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();



  factory NotificationService() {
    return _notificationService;
  }

  NotificationService._internal();

  /*selectNotification(String payload) async {
    if (kDebugMode) {
      print('Notification clicked');
    }
    return Future.value(0);
  }*/

  selectNotification(payload) {
    if (payload != null) {
      debugPrint('notification payload: $payload');
    }
  }

  Future<void> init() async {
    const AndroidInitializationSettings initializationSettingsAndroid = AndroidInitializationSettings('app_icon');

    final DarwinInitializationSettings initializationSettingsIOS =
    DarwinInitializationSettings(
      requestSoundPermission: true,
      requestBadgePermission: true,
      requestAlertPermission: true,
      onDidReceiveLocalNotification: onDidReceiveLocalNotification,
    );

    final InitializationSettings initializationSettings =
    InitializationSettings(
        android: initializationSettingsAndroid,
        iOS: initializationSettingsIOS,
        macOS: null);




    await flutterLocalNotificationsPlugin.initialize(initializationSettings, onDidReceiveNotificationResponse: selectNotification);

    final bool? result = await flutterLocalNotificationsPlugin
        .resolvePlatformSpecificImplementation<
        IOSFlutterLocalNotificationsPlugin>()
        ?.requestPermissions(
      alert: true,
      badge: true,
      sound: true,
    );
  }


  Future onDidReceiveLocalNotification(
      int id, String? title, String? body, String? payload) async {
    return Future.value(1);
  }

  final NotificationDetails platformChannelSpecifics =
  NotificationDetails(
      iOS: DarwinNotificationDetails(
        presentAlert: true,
        presentBadge: true,
        presentSound: true,
      ),
      android: const AndroidNotificationDetails(
        'channel_id','channel_name',
        channelDescription: "Channel description",
        importance: Importance.high,
        priority: Priority.max,
        playSound: true,
      )
  );

  Future showNotification({
    required int id, required String title, required String body
  }) async{
    final details= await platformChannelSpecifics;
    await flutterLocalNotificationsPlugin.show(id, title, body, details);
  }

}