import 'package:plugin_platform_interface/plugin_platform_interface.dart';

import 'onygoutils_method_channel.dart';

abstract class OnygoutilsPlatform extends PlatformInterface {
  /// Constructs a OnygoutilsPlatform.
  OnygoutilsPlatform() : super(token: _token);

  static final Object _token = Object();

  static OnygoutilsPlatform _instance = MethodChannelOnygoutils();

  /// The default instance of [OnygoutilsPlatform] to use.
  ///
  /// Defaults to [MethodChannelOnygoutils].
  static OnygoutilsPlatform get instance => _instance;
  
  /// Platform-specific implementations should set this with their own
  /// platform-specific class that extends [OnygoutilsPlatform] when
  /// they register themselves.
  static set instance(OnygoutilsPlatform instance) {
    PlatformInterface.verifyToken(instance, _token);
    _instance = instance;
  }

  Future<String?> getPlatformVersion() {
    throw UnimplementedError('platformVersion() has not been implemented.');
  }
}
