

import 'package:intl/intl.dart';

import 'onygoutils_platform_interface.dart';

class Onygoutils {

  Future<String?> getPlatformVersion() {
    return OnygoutilsPlatform.instance.getPlatformVersion();
  }


  String formmatter(DateTime value, {format ='HH:mm:ss'}){
    var formatter=DateFormat(format);
    String stringdate=formatter.format(value);

    return stringdate;
  }

  String strDigits(int n) => n.toString().padLeft(2, '0');

  setStringDate(Duration myD) {

    String hourLeft =
    strDigits(myD.inHours.remainder(24));

    String minuteLeft =
    strDigits(myD.inMinutes.remainder(60));
    String secondsLeft =
    strDigits(myD.inSeconds.remainder(60));
    return "$hourLeft:$minuteLeft:$secondsLeft";
  }
}
