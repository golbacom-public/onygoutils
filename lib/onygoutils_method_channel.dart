import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';

import 'onygoutils_platform_interface.dart';

/// An implementation of [OnygoutilsPlatform] that uses method channels.
class MethodChannelOnygoutils extends OnygoutilsPlatform {
  /// The method channel used to interact with the native platform.
  @visibleForTesting
  final methodChannel = const MethodChannel('onygoutils');

  @override
  Future<String?> getPlatformVersion() async {
    final version = await methodChannel.invokeMethod<String>('getPlatformVersion');
    return version;
  }
}
