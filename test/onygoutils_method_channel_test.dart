import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:onygoutils/onygoutils_method_channel.dart';

void main() {
  MethodChannelOnygoutils platform = MethodChannelOnygoutils();
  const MethodChannel channel = MethodChannel('onygoutils');

  TestWidgetsFlutterBinding.ensureInitialized();

  setUp(() {
    channel.setMockMethodCallHandler((MethodCall methodCall) async {
      return '42';
    });
  });

  tearDown(() {
    channel.setMockMethodCallHandler(null);
  });

  test('getPlatformVersion', () async {
    expect(await platform.getPlatformVersion(), '42');
  });
}
