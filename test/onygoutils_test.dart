import 'package:flutter_test/flutter_test.dart';
import 'package:onygoutils/onygoutils.dart';
import 'package:onygoutils/onygoutils_platform_interface.dart';
import 'package:onygoutils/onygoutils_method_channel.dart';
import 'package:plugin_platform_interface/plugin_platform_interface.dart';

class MockOnygoutilsPlatform 
    with MockPlatformInterfaceMixin
    implements OnygoutilsPlatform {

  @override
  Future<String?> getPlatformVersion() => Future.value('42');
}

void main() {
  final OnygoutilsPlatform initialPlatform = OnygoutilsPlatform.instance;

  test('$MethodChannelOnygoutils is the default instance', () {
    expect(initialPlatform, isInstanceOf<MethodChannelOnygoutils>());
  });

  test('getPlatformVersion', () async {
    Onygoutils onygoutilsPlugin = Onygoutils();
    MockOnygoutilsPlatform fakePlatform = MockOnygoutilsPlatform();
    OnygoutilsPlatform.instance = fakePlatform;
  
    expect(await onygoutilsPlugin.getPlatformVersion(), '42');
  });
}
