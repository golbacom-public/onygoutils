#import "OnygoutilsPlugin.h"
#if __has_include(<onygoutils/onygoutils-Swift.h>)
#import <onygoutils/onygoutils-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "onygoutils-Swift.h"
#endif

@implementation OnygoutilsPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftOnygoutilsPlugin registerWithRegistrar:registrar];
}
@end
